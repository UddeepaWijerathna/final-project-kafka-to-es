package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import services.ConsumerService;
import services.MessageService;
import services.ProducerService;
import services.dto.StatusAndMinAndHourAndDateAggregator;

import java.io.IOException;
import java.util.List;

public class KafkaController extends Controller {
    @Inject
    public ProducerService producerService;

    @Inject
    public ConsumerService consumerService;

    @Inject
    public MessageService messageService;

    public Result postMessage(Http.Request request) throws IOException {
        //JsonNode res = messageService.sendMessageToEs(producerService.postMessageToKafka(request.body().asJson()));
        JsonNode res = consumerService.subscribeMessage(producerService.sendMessage(request.body().asJson()));
        return ok(Json.toJson(res));
    }

    public Result aggregateSync() throws Exception {
        JsonNode results = messageService.aggregateSync();
        return Results.ok(Json.toJson(results));
    }


}
