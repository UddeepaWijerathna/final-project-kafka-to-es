package services.dto;

import java.util.List;

public class StatusAndMinAggregator {
    public long count;
    public String key;
    public List<StatusAggregator> values;

    public StatusAndMinAggregator(long count, String key, List<StatusAggregator> values) {
        this.count = count;
        this.key = key;
        this.values = values;
    }

    public StatusAndMinAggregator(){}

    @Override
    public String toString() {
        return "DateAndStatusAggregator{" +
                "count=" + count +
                ", key='" + key + '\'' +
                ", values=" + values +
                '}';
    }
}
