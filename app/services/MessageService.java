package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import play.libs.Json;
import services.dto.StatusAggregator;
import services.dto.StatusAndMinAggregator;
import services.dto.StatusAndMinAndHourAggregator;
import services.dto.StatusAndMinAndHourAndDateAggregator;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MessageService {
    private final String INDEX_NAME = "es-kafka-qc";
    private final String DOC_TYPE = "message";

    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost("localhost", 9200));
    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );

    public JsonNode aggregateSync() throws Exception {

        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        int hour = localTime.getHour();
        int minute = localTime.getMinute();

        int gteHour,ltHour;
        int gteMinute =  minute -1;

        if(minute==0){
            gteHour= hour-2;
            ltHour = hour-1;

        }else{
            gteHour= hour-1;
            ltHour = hour;
        }
        System.out.println(gteMinute);
        SearchSourceBuilder query = new SearchSourceBuilder()
                .query(QueryBuilders.boolQuery().filter(
                        QueryBuilders.termQuery("date", String.valueOf(localDate))
                ).must(QueryBuilders.rangeQuery("hour").gte(gteHour).lt(ltHour)
                ).must(QueryBuilders.rangeQuery("min").gte(gteMinute)))
                .aggregation(
                        AggregationBuilders.terms("DATE_AGGREGATION").field("date")
                                .subAggregation(AggregationBuilders.terms("HOUR_AGGREGATION").field("hour")
                                        .subAggregation(AggregationBuilders.terms("MIN_AGGREGATION").field("min")
                                                .subAggregation(AggregationBuilders.terms("STATUS_AGGREGATION").field("status")
                                                )))
        );

        SearchRequest request = new SearchRequest(INDEX_NAME).types(DOC_TYPE).source(query.from(0).size(0));

        SearchResponse response = highLevelClient.search(request, RequestOptions.DEFAULT);

        List<StatusAndMinAndHourAndDateAggregator> resList = aggregationParser(response);

        List<JsonNode> objectsList = new ArrayList<>();
        for (StatusAndMinAndHourAndDateAggregator ob: resList) {
            if(ob.key.equals(localDate.toString())){
                List<StatusAndMinAndHourAggregator> dateStatusHourList = ob.values;
                for (StatusAndMinAndHourAggregator ob1: dateStatusHourList) {

                    List<StatusAndMinAggregator> l1 = ob1.values;
                    for (StatusAndMinAggregator ob2: l1) {
                        // minute level
                        System.out.println(ob2.key); //minute
                        System.out.println(ob2.count); // per minute count total pass fail
                        List<StatusAggregator> l2 = ob2.values;

                        ObjectNode object = Json.newObject();
                        int passCount = 0;
                        int failCount = 0;
                        object.put("minute", ob2.key);

                        for (StatusAggregator x: l2) {

                            if (x.key.equals("pass")){
                                object.put("passCount", x.count);
                                passCount = (int) x.count;
                            }
                            else {
                                object.put("failCount", x.count);
                                failCount = (int) x.count;
                            }

                            object.put("passCount", passCount);
                            object.put("failCount", failCount);

                        }

                        objectsList.add(object);
                    }
                }
            }
        }
        System.out.println(objectsList);
        List<Integer> minuteList = new ArrayList<>();
        List<Integer> failCountList = new ArrayList<>();
        List<Integer> passCountList = new ArrayList<>();

        for (JsonNode ob:objectsList) {
            minuteList.add(ob.get("minute").asInt());
            passCountList.add(ob.get("passCount").asInt());
            failCountList.add(ob.get("failCount").asInt());
        }
        System.out.println(minuteList);
        ObjectNode finalResult = Json.newObject();
        int passCount = passCountList.stream().reduce(0, Integer::sum);
        int failCount = failCountList.stream().reduce(0, Integer::sum);

        finalResult.putPOJO("minutes", minuteList);
        finalResult.putPOJO("pass", passCountList);
        finalResult.put("passCount", passCount);
        finalResult.putPOJO("fail", failCountList);
        finalResult.put("failCount", failCount);
        finalResult.put("date", localDate.toString());

        String timeDuration1 = gteHour < 12 ? "AM" : "PM";
        String timeDuration2 = ltHour < 12 ? "AM" : "PM";
        String minutes = gteMinute > 10 ? String.valueOf(gteMinute) : gteMinute > 0 ?"0"+(gteMinute): "59";
        finalResult.put("prevTime", (gteHour)+":"+(minutes)+timeDuration1);
        finalResult.put("nextTime",ltHour+":"+(minutes)+timeDuration2);

        System.out.println(finalResult);
        return finalResult;



    }

    /**
     * Function to parser SearchResponse aggregations into JAVA DTO which can be directly serialize to JSON data in API response
     */
    private List<StatusAndMinAndHourAndDateAggregator> aggregationParser(SearchResponse searchResponse) {

        List<StatusAndMinAndHourAndDateAggregator> valuesStatusAndMinAndHourAndDateAggregator = new ArrayList<>();

        MultiBucketsAggregation aggregations = searchResponse.getAggregations().get("DATE_AGGREGATION");


        for (MultiBucketsAggregation.Bucket bucket : aggregations.getBuckets()) {
            String dateKey = bucket.getKey().toString();
            long dateCount = bucket.getDocCount();

            MultiBucketsAggregation internalHourBuckets = bucket.getAggregations().get("HOUR_AGGREGATION");


            List<StatusAndMinAndHourAggregator> valuesStatusAndMinAndHourAggregator = new ArrayList<>();

            for (MultiBucketsAggregation.Bucket internalStatusBucket : internalHourBuckets.getBuckets()) {
                String hourKey = internalStatusBucket.getKey().toString();
                long hourCount = internalStatusBucket.getDocCount();


                MultiBucketsAggregation internalMinBuckets = internalStatusBucket.getAggregations().get("MIN_AGGREGATION");

                List<StatusAndMinAggregator> valuesStatusAndMinAggregator = new ArrayList<>();

                for (MultiBucketsAggregation.Bucket internalHourBucket : internalMinBuckets.getBuckets()) {
                    String minKey = internalHourBucket.getKey().toString();
                    long minCount = internalHourBucket.getDocCount();


                    MultiBucketsAggregation internalStatusBuckets = internalHourBucket.getAggregations().get("STATUS_AGGREGATION");


                    List<StatusAggregator> valuesStatusAggregator = new ArrayList<>();
                    for (MultiBucketsAggregation.Bucket internalMinBucket : internalStatusBuckets.getBuckets()) {
                        String statusKey = internalMinBucket.getKey().toString();
                        long statusCount = internalMinBucket.getDocCount();


                        valuesStatusAggregator.add(new StatusAggregator(statusCount, statusKey));

                    }
                    valuesStatusAndMinAggregator.add(new StatusAndMinAggregator(minCount, minKey, valuesStatusAggregator));

                }
                valuesStatusAndMinAndHourAggregator.add(new StatusAndMinAndHourAggregator(hourCount, hourKey, valuesStatusAndMinAggregator));
            }
            valuesStatusAndMinAndHourAndDateAggregator.add(new StatusAndMinAndHourAndDateAggregator(dateCount, dateKey, valuesStatusAndMinAndHourAggregator));

        }

        return valuesStatusAndMinAndHourAndDateAggregator;
    }
}
