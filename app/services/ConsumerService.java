package services;

import bo.Message;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import play.libs.Json;
import services.dto.KafkaJsonDeserializer;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;


public class ConsumerService {
    public static String TOPIC = "test-kafka-topic";
    private final String INDEX_NAME = "es-kafka-qc";
    private final String DOC_TYPE = "message";
    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost("localhost", 9200));
    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );

    public JsonNode subscribeMessage(JsonNode body) {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        properties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100);
        ObjectNode result = Json.newObject();
        KafkaConsumer<String, Message> consumer =
                new KafkaConsumer<>(
                        properties,
                        new StringDeserializer(),
                        new KafkaJsonDeserializer<Message>(Message.class));
        // Subscribe to the topic
        consumer.subscribe(Collections.singletonList(TOPIC));
        try {
                ConsumerRecords<String, Message> consumerRecords = consumer.poll(Duration.ofMillis(1000));
                consumerRecords.forEach(record -> {
                    int id = body.get("id").asInt();
                    String status = body.get("status").asText();
                    String date = body.get("date").asText();
                    int hour = body.get("hour").asInt();
                    int min = body.get("min").asInt();
                    Message message = new Message(id, status, date, hour, min);
                    IndexRequest request = new IndexRequest(INDEX_NAME).type(DOC_TYPE)
                            .source(Json.stringify(Json.toJson(message)), XContentType.JSON);
                    IndexResponse response = null;

                    try {
                        response = highLevelClient.index(request, RequestOptions.DEFAULT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //System.out.println(response.status());

                    if (response.getIndex() != null && response.status() == RestStatus.CREATED) {
                        result.put("message", "Message Created ID: " + message.getId());
                    } else {
                        result.put("message", "Message Not Created");
                    }

                });
                consumer.commitSync();

        } finally {

            consumer.close();
            return result;

        }

    }

}
