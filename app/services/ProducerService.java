package services;

import bo.Message;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import play.libs.Json;

import java.util.Properties;

public class ProducerService {

    public ProducerService() {
    }


    public JsonNode sendMessage(JsonNode body) {

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        KafkaProducer<String, Message> producer =
                new KafkaProducer<>(
                        props,
                        new StringSerializer(),
                        new KafkaJsonSerializer()

                );

        String TOPIC = "test-kafka-topic";

        int id = body.get("id").asInt();
        String status = body.get("status").asText();
        String date = body.get("date").asText();
        int hour = body.get("hour").asInt();
        int min = body.get("min").asInt();
        Message message = new Message(id, status, date, hour, min);

        ProducerRecord<String, Message> record = new ProducerRecord<>(TOPIC, message);
        try {
            producer.send(record);
        } catch (Exception e) {

            e.printStackTrace();
        }
        producer.flush();
        producer.close();

        return Json.toJson(message);
    }


}
